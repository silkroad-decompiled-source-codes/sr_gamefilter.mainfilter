﻿using SmartAssembly.Attributes;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: Extension]
[assembly: AssemblyTitle("SR_GameFilter.MainFilter")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SR_GameFilter.MainFilter")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("11f71d13-769e-4363-9e51-d050e3f4ff16")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: PoweredBy("Powered by SmartAssembly 7.4.0.3402")]
[assembly: SuppressIldasm]
[assembly: AssemblyVersion("1.0.0.0")]
