﻿// Decompiled with Syinea's decompiler
// Type: 2A423115
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

internal struct \u0032A423115
{
  private const int \u00303E96675 = 11;
  private const uint \u00323093310 = 2048;
  private const int \u0031A64241E = 5;
  private uint \u00375FF68A0;

  public void \u00337A10F61()
  {
    this.\u00375FF68A0 = 1024U;
  }

  public uint \u00350A9532A(\u0033CE4593A _param1)
  {
    uint num = (_param1.\u003510C1E1D >> 11) * this.\u00375FF68A0;
    if (_param1.\u0033D004804 < num)
    {
      _param1.\u003510C1E1D = num;
      this.\u00375FF68A0 += 2048U - this.\u00375FF68A0 >> 5;
      if (_param1.\u003510C1E1D < 16777216U)
      {
        _param1.\u0033D004804 = _param1.\u0033D004804 << 8 | (uint) (byte) _param1.\u003421C4BD9.ReadByte();
        _param1.\u003510C1E1D <<= 8;
      }
      return 0;
    }
    _param1.\u003510C1E1D -= num;
    _param1.\u0033D004804 -= num;
    this.\u00375FF68A0 -= this.\u00375FF68A0 >> 5;
    if (_param1.\u003510C1E1D < 16777216U)
    {
      _param1.\u0033D004804 = _param1.\u0033D004804 << 8 | (uint) (byte) _param1.\u003421C4BD9.ReadByte();
      _param1.\u003510C1E1D <<= 8;
    }
    return 1;
  }
}
