﻿// Decompiled with Syinea's decompiler
// Type: 60EF36D8
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System;
using System.IO;

public class \u00360EF36D8
{
  private uint \u0037E775FB1 = 1;
  private readonly \u003037B2395 \u0033E810588 = new \u003037B2395();
  private readonly \u0033CE4593A \u003053F4A22 = new \u0033CE4593A();
  private readonly \u0032A423115[] \u00328165E52 = new \u0032A423115[192];
  private readonly \u0032A423115[] \u003098223BA = new \u0032A423115[12];
  private readonly \u0032A423115[] \u003316A2444 = new \u0032A423115[12];
  private readonly \u0032A423115[] \u003252A4E8D = new \u0032A423115[12];
  private readonly \u0032A423115[] \u00364D266A7 = new \u0032A423115[12];
  private readonly \u0032A423115[] \u0035F6855D4 = new \u0032A423115[192];
  private readonly \u00377A076E5[] \u0037E311928 = new \u00377A076E5[4];
  private readonly \u0032A423115[] \u003274C4E36 = new \u0032A423115[114];
  private \u00377A076E5 \u0035A261637 = new \u00377A076E5(4);
  private readonly \u00360EF36D8.\u00330272EBA \u0035C930B47 = new \u00360EF36D8.\u00330272EBA();
  private readonly \u00360EF36D8.\u00330272EBA \u003658420AE = new \u00360EF36D8.\u00330272EBA();
  private readonly \u00360EF36D8.\u003267267A6 \u0035AC04E4F = new \u00360EF36D8.\u003267267A6();
  private uint \u003210328B3;
  private uint \u0033A6E524C;
  private uint \u0030D3557DF;

  public \u00360EF36D8()
  {
    this.\u003210328B3 = uint.MaxValue;
    for (int index = 0; index < 4; ++index)
      this.\u0037E311928[index] = new \u00377A076E5(6);
  }

  private void \u003123C0024(uint _param1)
  {
    if ((int) this.\u003210328B3 == (int) _param1)
      return;
    this.\u003210328B3 = _param1;
    this.\u0033A6E524C = Math.Max(this.\u003210328B3, 1U);
    this.\u0033E810588.\u003516A2AEE(Math.Max(this.\u0033A6E524C, 4096U));
  }

  private void \u0032FE5072A(int _param1, int _param2)
  {
    if (_param1 > 8)
      throw new ArgumentException("lp > 8");
    if (_param2 > 8)
      throw new ArgumentException("lc > 8");
    this.\u0035AC04E4F.\u0032C93189E(_param1, _param2);
  }

  private void \u00314F31B40(int _param1)
  {
    if (_param1 > 4)
      throw new ArgumentException("pb > Base.KNumPosStatesBitsMax");
    uint num = (uint) (1 << _param1);
    this.\u0035C930B47.\u003491C1296(num);
    this.\u003658420AE.\u003491C1296(num);
    this.\u0030D3557DF = num - 1U;
  }

  private void \u0037A0F466D(Stream _param1, Stream _param2)
  {
    this.\u003053F4A22.\u00312BB28A0(_param1);
    this.\u0033E810588.\u0030D0B13A8(_param2, false);
    for (uint index1 = 0; index1 < 12U; ++index1)
    {
      for (uint index2 = 0; index2 <= this.\u0030D3557DF; ++index2)
      {
        uint num = (index1 << 4) + index2;
        this.\u00328165E52[(int) num].\u00337A10F61();
        this.\u0035F6855D4[(int) num].\u00337A10F61();
      }
      this.\u003098223BA[(int) index1].\u00337A10F61();
      this.\u003316A2444[(int) index1].\u00337A10F61();
      this.\u003252A4E8D[(int) index1].\u00337A10F61();
      this.\u00364D266A7[(int) index1].\u00337A10F61();
    }
    this.\u0035AC04E4F.\u00366BB3D9B();
    for (uint index = 0; index < 4U; ++index)
      this.\u0037E311928[(int) index].\u00311776113();
    for (uint index = 0; index < 114U; ++index)
      this.\u003274C4E36[(int) index].\u00337A10F61();
    this.\u0035C930B47.\u0031B73036F();
    this.\u003658420AE.\u0031B73036F();
    this.\u0035A261637.\u00311776113();
  }

  public void \u0032D204312(Stream _param1, Stream _param2, long _param3)
  {
    this.\u0037A0F466D(_param1, _param2);
    \u003117076A5.\u0034E4866AF obj = new \u003117076A5.\u0034E4866AF();
    obj.\u0033D336971();
    uint num1 = 0;
    uint num2 = 0;
    uint num3 = 0;
    uint num4 = 0;
    ulong num5 = 0;
    ulong num6 = (ulong) _param3;
    if (num5 < num6)
    {
      if (this.\u00328165E52[(int) obj.\u0034F7803B9 << 4].\u00350A9532A(this.\u003053F4A22) != 0U)
        throw new InvalidDataException("IsMatchDecoders");
      obj.\u00368876648();
      this.\u0033E810588.\u00316DA69A4(this.\u0035AC04E4F.\u003655A6AD0(this.\u003053F4A22, 0U, (byte) 0));
      ++num5;
    }
    while (num5 < num6)
    {
      uint num7 = (uint) num5 & this.\u0030D3557DF;
      if (this.\u00328165E52[((int) obj.\u0034F7803B9 << 4) + (int) num7].\u00350A9532A(this.\u003053F4A22) == 0U)
      {
        byte num8 = this.\u0033E810588.\u003247250F8(0U);
        this.\u0033E810588.\u00316DA69A4(obj.\u00348754900() ? this.\u0035AC04E4F.\u003655A6AD0(this.\u003053F4A22, (uint) num5, num8) : this.\u0035AC04E4F.\u00375EE0C83(this.\u003053F4A22, (uint) num5, num8, this.\u0033E810588.\u003247250F8(num1)));
        obj.\u00368876648();
        ++num5;
      }
      else
      {
        uint num8;
        if (this.\u003098223BA[(int) obj.\u0034F7803B9].\u00350A9532A(this.\u003053F4A22) == 1U)
        {
          if (this.\u003316A2444[(int) obj.\u0034F7803B9].\u00350A9532A(this.\u003053F4A22) == 0U)
          {
            if (this.\u0035F6855D4[((int) obj.\u0034F7803B9 << 4) + (int) num7].\u00350A9532A(this.\u003053F4A22) == 0U)
            {
              obj.\u00362F4413D();
              this.\u0033E810588.\u00316DA69A4(this.\u0033E810588.\u003247250F8(num1));
              ++num5;
              continue;
            }
          }
          else
          {
            uint num9;
            if (this.\u003252A4E8D[(int) obj.\u0034F7803B9].\u00350A9532A(this.\u003053F4A22) == 0U)
            {
              num9 = num2;
            }
            else
            {
              if (this.\u00364D266A7[(int) obj.\u0034F7803B9].\u00350A9532A(this.\u003053F4A22) == 0U)
              {
                num9 = num3;
              }
              else
              {
                num9 = num4;
                num4 = num3;
              }
              num3 = num2;
            }
            num2 = num1;
            num1 = num9;
          }
          num8 = this.\u003658420AE.\u0036C74146C(this.\u003053F4A22, num7) + 2U;
          obj.\u00326347EED();
        }
        else
        {
          num4 = num3;
          num3 = num2;
          num2 = num1;
          num8 = 2U + this.\u0035C930B47.\u0036C74146C(this.\u003053F4A22, num7);
          obj.\u00328AB6880();
          uint num9 = this.\u0037E311928[(int) \u003117076A5.\u003778E182A(num8)].\u0031AF91F08(this.\u003053F4A22);
          if (num9 >= 4U)
          {
            int num10 = (int) (num9 >> 1) - 1;
            uint num11 = (uint) ((2 | (int) num9 & 1) << num10);
            num1 = num9 >= 14U ? num11 + (this.\u003053F4A22.\u0033AB80CDB(num10 - 4) << 4) + this.\u0035A261637.\u00339C70037(this.\u003053F4A22) : num11 + \u00377A076E5.\u00306934D67(this.\u003274C4E36, (uint) ((int) num11 - (int) num9 - 1), this.\u003053F4A22, num10);
          }
          else
            num1 = num9;
        }
        if ((ulong) num1 >= (ulong) this.\u0033E810588.\u0032FA3211E + num5 || num1 >= this.\u0033A6E524C)
        {
          if (num1 != uint.MaxValue)
            throw new InvalidDataException("rep0");
          break;
        }
        this.\u0033E810588.\u00353715B56(num1, num8);
        num5 += (ulong) num8;
      }
    }
    this.\u0033E810588.\u00300932405();
    this.\u0033E810588.\u0034E25741E();
    this.\u003053F4A22.\u00315EA1E91();
  }

  public void \u0035ACE1FD6(byte[] _param1)
  {
    if (_param1.Length < 5)
      throw new ArgumentException("properties.Length < 5");
    int num1 = (int) _param1[0] % 9;
    int num2 = (int) _param1[0] / 9;
    int num3 = num2 % 5;
    int num4 = num2 / 5;
    if (num4 > 4)
      throw new ArgumentException("pb > Base.kNumPosStatesBitsMax");
    uint num5 = 0;
    for (int index = 0; index < 4; ++index)
      num5 += (uint) _param1[1 + index] << index * 8;
    this.\u003123C0024(num5);
    this.\u0032FE5072A(num3, num1);
    this.\u00314F31B40(num4);
  }

  private class \u00330272EBA
  {
    private readonly \u00377A076E5[] \u00304E0213D = new \u00377A076E5[16];
    private readonly \u00377A076E5[] \u0037EA92D48 = new \u00377A076E5[16];
    private \u00377A076E5 \u003717706B5 = new \u00377A076E5(8);
    private \u0032A423115 \u0034DE51641;
    private \u0032A423115 \u003142960EC;
    private uint \u0036BC52FAA;

    public void \u003491C1296(uint _param1)
    {
      for (uint index = this.\u0036BC52FAA; index < _param1; ++index)
      {
        this.\u00304E0213D[(int) index] = new \u00377A076E5(3);
        this.\u0037EA92D48[(int) index] = new \u00377A076E5(3);
      }
      this.\u0036BC52FAA = _param1;
    }

    public void \u0031B73036F()
    {
      this.\u0034DE51641.\u00337A10F61();
      for (uint index = 0; index < this.\u0036BC52FAA; ++index)
      {
        this.\u00304E0213D[(int) index].\u00311776113();
        this.\u0037EA92D48[(int) index].\u00311776113();
      }
      this.\u003142960EC.\u00337A10F61();
      this.\u003717706B5.\u00311776113();
    }

    public uint \u0036C74146C(\u0033CE4593A _param1, uint _param2)
    {
      if (this.\u0034DE51641.\u00350A9532A(_param1) == 0U)
        return this.\u00304E0213D[(int) _param2].\u0031AF91F08(_param1);
      uint num = 8;
      return this.\u003142960EC.\u00350A9532A(_param1) != 0U ? num + 8U + this.\u003717706B5.\u0031AF91F08(_param1) : num + this.\u0037EA92D48[(int) _param2].\u0031AF91F08(_param1);
    }
  }

  private class \u003267267A6
  {
    private uint \u00357170348 = 1;
    private \u00360EF36D8.\u003267267A6.\u00342F8389A[] \u00362192B6C;
    private int \u0032E511879;
    private int \u00313280299;
    private uint \u00334FA7E26;

    public void \u0032C93189E(int _param1, int _param2)
    {
      if (this.\u00362192B6C != null && this.\u0032E511879 == _param2 && this.\u00313280299 == _param1)
        return;
      this.\u00313280299 = _param1;
      this.\u00334FA7E26 = (uint) ((1 << _param1) - 1);
      this.\u0032E511879 = _param2;
      uint num = (uint) (1 << this.\u0032E511879 + this.\u00313280299);
      this.\u00362192B6C = new \u00360EF36D8.\u003267267A6.\u00342F8389A[(int) num];
      for (uint index = 0; index < num; ++index)
        this.\u00362192B6C[(int) index].\u0031A2A3286();
    }

    public void \u00366BB3D9B()
    {
      uint num = (uint) (1 << this.\u0032E511879 + this.\u00313280299);
      for (uint index = 0; index < num; ++index)
        this.\u00362192B6C[(int) index].\u00364FA02F3();
    }

    private uint \u0034D8217A7(uint _param1, byte _param2)
    {
      return (uint) ((((int) _param1 & (int) this.\u00334FA7E26) << this.\u0032E511879) + ((int) _param2 >> 8 - this.\u0032E511879));
    }

    public byte \u003655A6AD0(\u0033CE4593A _param1, uint _param2, byte _param3)
    {
      return this.\u00362192B6C[(int) this.\u0034D8217A7(_param2, _param3)].\u003513B5538(_param1);
    }

    public byte \u00375EE0C83(\u0033CE4593A _param1, uint _param2, byte _param3, byte _param4)
    {
      return this.\u00362192B6C[(int) this.\u0034D8217A7(_param2, _param3)].\u003066D5E3B(_param1, _param4);
    }

    private struct \u00342F8389A
    {
      private \u0032A423115[] \u003590C3DA5;

      public void \u0031A2A3286()
      {
        this.\u003590C3DA5 = new \u0032A423115[768];
      }

      public void \u00364FA02F3()
      {
        for (int index = 0; index < 768; ++index)
          this.\u003590C3DA5[index].\u00337A10F61();
      }

      public byte \u003513B5538(\u0033CE4593A _param1)
      {
        uint num = 1;
        do
        {
          num = num << 1 | this.\u003590C3DA5[(int) num].\u00350A9532A(_param1);
        }
        while (num < 256U);
        return (byte) num;
      }

      public byte \u003066D5E3B(\u0033CE4593A _param1, byte _param2)
      {
        uint num1 = 1;
        do
        {
          uint num2 = (uint) ((int) _param2 >> 7 & 1);
          _param2 <<= 1;
          uint num3 = this.\u003590C3DA5[(1 + (int) num2 << 8) + (int) num1].\u00350A9532A(_param1);
          num1 = num1 << 1 | num3;
          if ((int) num2 != (int) num3)
          {
            while (num1 < 256U)
              num1 = num1 << 1 | this.\u003590C3DA5[(int) num1].\u00350A9532A(_param1);
            break;
          }
        }
        while (num1 < 256U);
        return (byte) num1;
      }
    }
  }
}
