﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0012;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0014
{
  [Guid("8267BBE3-F890-491C-B7B6-2DB1EF0E5D2B")]
  [TypeLibType(4160)]
  [ComImport]
  public interface \u0001
  {
    [DispId(3)]
    \u0001 Rules { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001([MarshalAs(UnmanagedType.BStr), In] string _param1, [MarshalAs(UnmanagedType.BStr), In] string _param2, [In] bool _param3, [In] bool _param4);

    [MethodImpl(MethodImplOptions.InternalCall)]
    bool \u0001([MarshalAs(UnmanagedType.BStr), In] string _param1, [MarshalAs(UnmanagedType.BStr), In] string _param2);
  }
}
