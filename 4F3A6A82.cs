﻿// Decompiled with Syinea's decompiler
// Type: 4F3A6A82
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System;
using System.Runtime.InteropServices;
using System.Text;

internal static class \u0034F3A6A82
{
  public static readonly IntPtr \u0033E0B0DDA = new IntPtr(-1);
  public static readonly IntPtr \u003757C03D7 = IntPtr.Zero;
  public static readonly IntPtr \u00369B32343 = new IntPtr(-1);
  public const int \u0032F32271E = -2147483648;
  public const int \u0036AE149F5 = 3;
  public const int \u00366D47657 = 128;
  public const int \u003181B79D6 = 1;
  public const int \u00317BE6C30 = 2;

  [DllImport("kernel32", EntryPoint = "CreateFile", CharSet = CharSet.Auto, SetLastError = true)]
  public static extern IntPtr \u00371E60E72(
    string _param0,
    int _param1,
    int _param2,
    IntPtr _param3,
    int _param4,
    int _param5,
    IntPtr _param6);

  [DllImport("kernel32", EntryPoint = "CreateFileMapping", CharSet = CharSet.Auto, SetLastError = true)]
  public static extern IntPtr \u00325534D14(
    IntPtr _param0,
    IntPtr _param1,
    \u0034F3A6A82.\u00303AA0AE2 _param2,
    int _param3,
    int _param4,
    string _param5);

  [DllImport("kernel32", EntryPoint = "FlushViewOfFile", SetLastError = true)]
  public static extern bool \u0033AD30400(IntPtr _param0, int _param1);

  [DllImport("kernel32", EntryPoint = "MapViewOfFile", SetLastError = true)]
  public static extern IntPtr \u0036CE831D5(
    IntPtr _param0,
    \u0034F3A6A82.\u00352997C56 _param1,
    int _param2,
    int _param3,
    IntPtr _param4);

  [DllImport("kernel32", EntryPoint = "OpenFileMapping", CharSet = CharSet.Auto, SetLastError = true)]
  public static extern IntPtr \u0033D4F408B(int _param0, bool _param1, string _param2);

  [DllImport("kernel32", EntryPoint = "UnmapViewOfFile", SetLastError = true)]
  public static extern bool \u003688C3359(IntPtr _param0);

  [DllImport("kernel32", EntryPoint = "CloseHandle", SetLastError = true)]
  public static extern bool \u00342B15CDE(IntPtr _param0);

  [DllImport("kernel32", EntryPoint = "GetFileSize", SetLastError = true)]
  public static extern uint \u0032F6F2764(IntPtr _param0, IntPtr _param1);

  [DllImport("kernel32", EntryPoint = "VirtualAlloc", SetLastError = true)]
  public static extern IntPtr \u00337BB087E(
    IntPtr _param0,
    UIntPtr _param1,
    \u0034F3A6A82.\u003356E258F _param2,
    \u0034F3A6A82.\u00303AA0AE2 _param3);

  [DllImport("kernel32", EntryPoint = "VirtualFree")]
  public static extern bool \u003118C424F(IntPtr _param0, uint _param1, uint _param2);

  [DllImport("kernel32", EntryPoint = "VirtualProtect", SetLastError = true)]
  public static extern bool \u00352726833(
    IntPtr _param0,
    UIntPtr _param1,
    \u0034F3A6A82.\u00303AA0AE2 _param2,
    out \u0034F3A6A82.\u00303AA0AE2 _param3);

  [DllImport("kernel32", EntryPoint = "GetVolumeInformation")]
  public static extern bool \u0030C2309AD(
    string _param0,
    StringBuilder _param1,
    uint _param2,
    ref uint _param3,
    ref uint _param4,
    ref uint _param5,
    StringBuilder _param6,
    uint _param7);

  [DllImport("kernel32", EntryPoint = "IsDebuggerPresent")]
  public static extern bool \u0034A2D3DC3();

  [DllImport("kernel32", EntryPoint = "CheckRemoteDebuggerPresent")]
  public static extern bool \u003626A5A2E();

  [DllImport("ntdll", EntryPoint = "NtQueryInformationProcess")]
  public static extern int \u00355ED3D01(
    IntPtr _param0,
    int _param1,
    byte[] _param2,
    uint _param3,
    out uint _param4);

  public enum \u003356E258F : uint
  {
    \u0034BBF653B = 4096, // 0x00001000
    \u0036DBC2048 = 8192, // 0x00002000
  }

  public enum \u00303AA0AE2 : uint
  {
    \u003688D233C = 1,
    \u00364FF5B37 = 2,
    \u003533973A2 = 4,
    \u0036FCC3F0B = 8,
    \u00332487118 = 16, // 0x00000010
    \u00378C12C6A = 32, // 0x00000020
    \u0032CB1288A = 64, // 0x00000040
    \u00313A5784E = 256, // 0x00000100
  }

  public enum \u00352997C56 : uint
  {
    \u003506C5B0B = 1,
    \u00339880AF6 = 2,
    \u003582D5370 = 4,
    \u00302A1750B = 31, // 0x0000001F
  }

  public enum \u003350A29C9 : uint
  {
    \u0033D345255 = 536870912, // 0x20000000
    \u0032A860423 = 1073741824, // 0x40000000
    \u00315ED751B = 2147483648, // 0x80000000
  }

  public enum \u003089D2EE2
  {
    \u003577949D4 = 1,
    \u00311BC038F = 2,
    \u0031E24582B = 4,
    \u0033EF2757A = 8,
    \u00354F92223 = 16, // 0x00000010
    \u00358E17663 = 32, // 0x00000020
    \u003041E055B = 256, // 0x00000100
    \u003435B6BE6 = 512, // 0x00000200
    \u00365A713AD = 768, // 0x00000300
    \u0036F665B29 = 131078, // 0x00020006
    \u0036C0B0E0D = 131097, // 0x00020019
    \u00376A81D46 = 131097, // 0x00020019
    \u0033DC86021 = 983103, // 0x000F003F
  }

  internal static class \u0036AAC3EB5
  {
    public static UIntPtr \u0034E4E085C;
    public static UIntPtr \u0033BD567EF;

    static \u0036AAC3EB5()
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public static class \u0030DC664C1
  {
    [DllImport("advapi32", EntryPoint = "RegOpenKeyEx")]
    private static extern uint \u003717C14B5(
      UIntPtr _param0,
      string _param1,
      uint _param2,
      int _param3,
      out UIntPtr _param4);

    [DllImport("advapi32", EntryPoint = "RegCloseKey")]
    private static extern uint \u003137C4FBD(UIntPtr _param0);

    [DllImport("advapi32", EntryPoint = "RegQueryValueEx")]
    private static extern int \u0035B6C787A(
      UIntPtr _param0,
      string _param1,
      int _param2,
      ref uint _param3,
      StringBuilder _param4,
      ref uint _param5);

    [DllImport("advapi32", EntryPoint = "RegQueryInfoKey")]
    private static extern uint \u0031CAF4AE5(
      UIntPtr _param0,
      StringBuilder _param1,
      ref uint _param2,
      IntPtr _param3,
      IntPtr _param4,
      IntPtr _param5,
      IntPtr _param6,
      IntPtr _param7,
      IntPtr _param8,
      IntPtr _param9,
      IntPtr _param10,
      out long _param11);

    public static string \u00308F46920(UIntPtr _param0, string _param1, string _param2)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool \u0035C425FDD(UIntPtr _param0, string _param1, ref DateTime _param2)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool \u003363B6703(
      UIntPtr _param0,
      string _param1,
      \u0034F3A6A82.\u003089D2EE2 _param2,
      ref DateTime _param3)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string \u00372B40878(
      UIntPtr _param0,
      string _param1,
      \u0034F3A6A82.\u003089D2EE2 _param2,
      string _param3)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
