﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0017;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0013
{
  [Guid("39EB36E0-2097-40BD-8AF2-63A13B525362")]
  [TypeLibType(4160)]
  [ComImport]
  public interface \u0002 : IEnumerable
  {
    int Count { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

    [DispId(2)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.IUnknown)]
    object \u0001([MarshalAs(UnmanagedType.Interface), In] \u0001 _param1);

    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.Interface)]
    \u0001 \u0001([In] int _param1);

    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.CustomMarshaler, MarshalType = "System.Runtime.InteropServices.CustomMarshalers.EnumeratorToEnumVariantMarshaler")]
    IEnumerator \u0001();
  }
}
