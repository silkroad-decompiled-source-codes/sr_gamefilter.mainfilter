﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0006;
using \u0011;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0013
{
  [Guid("C0E9D7FA-E07E-430A-B19A-090CE82D92E2")]
  [TypeLibType(4160)]
  [ComImport]
  public interface \u0001 : IEnumerable
  {
    int Count { [MethodImpl(MethodImplOptions.InternalCall)] get; }

    [DispId(2)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    void Add([MarshalAs(UnmanagedType.Interface), In] \u0001 Port);

    [DispId(3)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001([In] int _param1, [In] \u0001 _param2);

    [DispId(4)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.Interface)]
    \u0001 \u0001([In] int _param1, [In] \u0001 _param2);

    [DispId(-4)]
    [TypeLibFunc(1)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.CustomMarshaler, MarshalType = "System.Runtime.InteropServices.CustomMarshalers.EnumeratorToEnumVariantMarshaler")]
    IEnumerator \u0001();
  }
}
