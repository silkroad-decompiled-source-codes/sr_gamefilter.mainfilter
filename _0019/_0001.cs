﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0004;
using \u0012;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0019
{
  [ComImport]
  public interface \u0001 : \u0002
  {
    new string Name { [MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    new string Description { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    new string ApplicationName { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    new string serviceName { [MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    new int Protocol { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    new string LocalPorts { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    [DispId(7)]
    new string RemotePorts { [MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    new string LocalAddresses { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    new string RemoteAddresses { [MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    [DispId(10)]
    new string IcmpTypesAndCodes { [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    [DispId(11)]
    new \u0002 Direction { [DispId(11), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(11), MethodImpl(MethodImplOptions.InternalCall)] set; }

    new object Interfaces { [MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Struct)] get; [DispId(12), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.Struct)] set; }

    new string InterfaceTypes { [DispId(13), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    [DispId(14)]
    new bool Enabled { [DispId(14), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(14), MethodImpl(MethodImplOptions.InternalCall)] set; }

    new string Grouping { [MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(15), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    new int Profiles { [DispId(16), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(17)]
    new bool EdgeTraversal { [DispId(17), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(17), MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(18)]
    new \u0005 Action { [MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(18), MethodImpl(MethodImplOptions.InternalCall)] set; }

    int EdgeTraversalOptions { [DispId(19), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
  }
}
