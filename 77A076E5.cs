﻿// Decompiled with Syinea's decompiler
// Type: 77A076E5
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

internal struct \u00377A076E5
{
  private readonly \u0032A423115[] \u0032E852D94;
  private readonly int \u00362D7762D;

  public \u00377A076E5(int _param1)
  {
    this.\u00362D7762D = _param1;
    this.\u0032E852D94 = new \u0032A423115[1 << _param1];
  }

  public void \u00311776113()
  {
    for (uint index = 1; (long) index < (long) (1 << this.\u00362D7762D); ++index)
      this.\u0032E852D94[(int) index].\u00337A10F61();
  }

  public uint \u0031AF91F08(\u0033CE4593A _param1)
  {
    uint num = 1;
    for (int index = this.\u00362D7762D; index > 0; --index)
      num = (num << 1) + this.\u0032E852D94[(int) num].\u00350A9532A(_param1);
    return num - (uint) (1 << this.\u00362D7762D);
  }

  public uint \u00339C70037(\u0033CE4593A _param1)
  {
    uint num1 = 1;
    uint num2 = 0;
    for (int index = 0; index < this.\u00362D7762D; ++index)
    {
      uint num3 = this.\u0032E852D94[(int) num1].\u00350A9532A(_param1);
      num1 = (num1 << 1) + num3;
      num2 |= num3 << index;
    }
    return num2;
  }

  public static uint \u00306934D67(
    \u0032A423115[] _param0,
    uint _param1,
    \u0033CE4593A _param2,
    int _param3)
  {
    uint num1 = 1;
    uint num2 = 0;
    for (int index = 0; index < _param3; ++index)
    {
      uint num3 = _param0[(int) _param1 + (int) num1].\u00350A9532A(_param2);
      num1 = (num1 << 1) + num3;
      num2 |= num3 << index;
    }
    return num2;
  }
}
