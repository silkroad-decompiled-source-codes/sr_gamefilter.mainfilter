﻿// Decompiled with Syinea's decompiler
// Type: 
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

internal delegate CallSiteBinder \u0086\u0002(
  [In] CSharpBinderFlags obj0,
  [In] string obj1,
  [In] IEnumerable<Type> obj2,
  [In] Type obj3,
  [In] IEnumerable<CSharpArgumentInfo> obj4);
