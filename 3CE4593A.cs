﻿// Decompiled with Syinea's decompiler
// Type: 3CE4593A
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.IO;

internal class \u0033CE4593A
{
  private uint \u00305FC05B9 = 1;
  public const uint \u0031DA85F56 = 16777216;
  public uint \u003510C1E1D;
  public uint \u0033D004804;
  public Stream \u003421C4BD9;

  public void \u00312BB28A0(Stream _param1)
  {
    this.\u003421C4BD9 = _param1;
    this.\u0033D004804 = 0U;
    this.\u003510C1E1D = uint.MaxValue;
    for (int index = 0; index < 5; ++index)
      this.\u0033D004804 = this.\u0033D004804 << 8 | (uint) (byte) this.\u003421C4BD9.ReadByte();
  }

  public void \u00315EA1E91()
  {
    this.\u003421C4BD9 = (Stream) null;
  }

  public uint \u0033AB80CDB(int _param1)
  {
    uint num1 = this.\u003510C1E1D;
    uint num2 = this.\u0033D004804;
    uint num3 = 0;
    for (int index = _param1; index > 0; --index)
    {
      num1 >>= 1;
      uint num4 = num2 - num1 >> 31;
      num2 -= num1 & num4 - 1U;
      num3 = (uint) ((int) num3 << 1 | 1 - (int) num4);
      if (num1 < 16777216U)
      {
        num2 = num2 << 8 | (uint) (byte) this.\u003421C4BD9.ReadByte();
        num1 <<= 8;
      }
    }
    this.\u003510C1E1D = num1;
    this.\u0033D004804 = num2;
    return num3;
  }
}
