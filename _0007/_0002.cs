﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0007;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0007
{
  [ComImport]
  public interface \u0002 : IEnumerable
  {
    [DispId(1)]
    int Count { [MethodImpl(MethodImplOptions.InternalCall)] get; }

    [MethodImpl(MethodImplOptions.InternalCall)]
    void Add([MarshalAs(UnmanagedType.Interface), In] \u0001 app);

    [DispId(3)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001([MarshalAs(UnmanagedType.BStr), In] string _param1);

    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.Interface)]
    \u0001 \u0001([MarshalAs(UnmanagedType.BStr), In] string _param1);

    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.CustomMarshaler, MarshalType = "System.Runtime.InteropServices.CustomMarshalers.EnumeratorToEnumVariantMarshaler")]
    IEnumerator \u0001();
  }
}
