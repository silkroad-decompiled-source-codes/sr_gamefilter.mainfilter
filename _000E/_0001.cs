﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u000E
{
  [Guid("A6207B2E-7CDD-426A-951E-5E1CBC5AFEAD")]
  [TypeLibType(4160)]
  [ComImport]
  public interface \u0001
  {
    [DispId(1)]
    bool AllowOutboundDestinationUnreachable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    bool AllowRedirect { [MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(3)]
    bool AllowInboundEchoRequest { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(4)]
    bool AllowOutboundTimeExceeded { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

    bool AllowOutboundParameterProblem { [MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(6)]
    bool AllowOutboundSourceQuench { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(7)]
    bool AllowInboundRouterRequest { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] set; }

    bool AllowInboundTimestampRequest { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(9)]
    bool AllowInboundMaskRequest { [MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(10)]
    bool AllowOutboundPacketTooBig { [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] set; }
  }
}
