﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.Protocol.Agent.ChatType
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

namespace SR_GameFilter.DataModel.Protocol.Agent
{
  public enum ChatType : byte
  {
    \u0001 = 1,
    \u0002 = 2,
    \u0003 = 3,
    \u0004 = 4,
    \u0005 = 5,
    \u0006 = 6,
    \u0007 = 7,
    \u0008 = 9,
    \u000E = 11, // 0x0B
    \u000F = 13, // 0x0D
    \u0010 = 16, // 0x10
  }
}
