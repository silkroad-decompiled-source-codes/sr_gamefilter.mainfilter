﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.Protocol.Agent.CharSelectionNameErrorCode
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

namespace SR_GameFilter.DataModel.Protocol.Agent
{
  public enum CharSelectionNameErrorCode : ushort
  {
    UIIT_MSG_GUILDERR_SAME_GUILDNAME_EXIST = 6,
    UIO_MSG_ERROR_ID = 6,
    UIIT_MSG_GUILD_NOT_CREATE = 7,
    UIO_SMERR_NOT_ALLOWED_CHARNAME = 7,
  }
}
