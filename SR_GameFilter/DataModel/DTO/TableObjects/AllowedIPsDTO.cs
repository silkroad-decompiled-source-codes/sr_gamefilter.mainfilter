﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.AllowedIPsDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects
{
  [DataContract]
  public sealed class AllowedIPsDTO
  {
    [DataMember(IsRequired = true, Name = "Allowed IPs")]
    public List<string> Addresses
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public AllowedIPsDTO(List<string> ips)
    {
      // ISSUE: unable to decompile the method.
    }

    public AllowedIPsDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
