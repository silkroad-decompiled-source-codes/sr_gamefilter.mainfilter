﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.RedirectionRuleArrayDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects
{
  [DataContract]
  public sealed class RedirectionRuleArrayDTO
  {
    [DataMember(IsRequired = true, Name = "Rules")]
    public List<RedirectionRuleDTO> Rules
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RedirectionRuleArrayDTO(List<RedirectionRuleDTO> rules)
    {
      // ISSUE: unable to decompile the method.
    }

    public RedirectionRuleArrayDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
