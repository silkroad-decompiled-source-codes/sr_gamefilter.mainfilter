﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.AgentGmsConfigDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects
{
  [DataContract]
  public sealed class AgentGmsConfigDTO
  {
    [DataMember(IsRequired = true, Name = "GM CharName")]
    public string User_ID
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string CharName
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool FindUser
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool MakeItem
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool LoadMonster
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool Zoe
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool MobKill
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool RecallUser
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool RecallGuild
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool MoveToUser
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool MoveToNpc
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool WP
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Totown
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool WorldStatus
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Invisible
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Invincible
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool Ban
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Spawnunique_loc
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool GoTotwn
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Command bool")]
    public bool MakeRentItem
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Unkown
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public AgentGmsConfigDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
