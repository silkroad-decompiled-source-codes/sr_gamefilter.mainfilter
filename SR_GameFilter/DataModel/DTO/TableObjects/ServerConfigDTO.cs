﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.ServerConfigDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SR_GameFilter.MainFilter.DataModel.Models;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects
{
  [DataContract]
  public sealed class ServerConfigDTO
  {
    [DataMember(IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Server name")]
    public string ServerName
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Server type")]
    public ServerType ServerType
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PacketsPerSec
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool BlockExploitViaFirewall
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ServerConfigDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
