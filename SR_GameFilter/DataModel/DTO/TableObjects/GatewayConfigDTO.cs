﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.GatewayConfigDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects
{
  [DataContract]
  public sealed class GatewayConfigDTO
  {
    private string \u003CModuleAddress\u003Ek__BackingField;
    private int \u003CModulePort\u003Ek__BackingField;

    public string ModuleAddress
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Module port")]
    public int ModulePort
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Enable auto captcha")]
    public bool EnableAutoCaptcha
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string AutoCaptchaValue
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GatewayConfigDTO(
      string ModuleAddress,
      int ModulePort,
      bool AutoCaptcha,
      string AutoCaptchaString)
    {
      // ISSUE: unable to decompile the method.
    }

    public GatewayConfigDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
