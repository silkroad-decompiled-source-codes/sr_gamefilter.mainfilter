﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.NetEngineConfigDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects
{
  [DataContract]
  public sealed class NetEngineConfigDTO
  {
    private string \u003CBindAddress\u003Ek__BackingField;
    private int \u003CBindPort\u003Ek__BackingField;
    private int \u003CConnTimeout\u003Ek__BackingField;
    private int \u003CMaxConns\u003Ek__BackingField;
    private int \u003CMaxConnsPerIp\u003Ek__BackingField;
    public int ContextPulseTimeout;

    [DataMember(IsRequired = true, Name = "Bind address")]
    public string BindAddress
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BindPort
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Connection timeout")]
    public int ConnTimeout
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Maximum connections")]
    public int MaxConns
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Max connections per IP")]
    public int MaxConnsPerIp
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PacketLogSize
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Buffer manager expansion step")]
    public int BufferManagerExpansionStep
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Buffer manager buffer size")]
    public int BufferManagerChunkSize
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Work processing task count")]
    public int WorkQueueProcessTasks
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public NetEngineConfigDTO(
      string BindAddress,
      int BindPort,
      int ConnectionTimeout,
      int MaximumConnections,
      int MaximumConnectionsPerIp,
      int ContextPulseTimeout,
      int PacketLogSize,
      int BufferManagerExpansionStep,
      int BufferManagerBufferSize,
      int WorkProcessTaskCount)
    {
      // ISSUE: unable to decompile the method.
    }

    public NetEngineConfigDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
