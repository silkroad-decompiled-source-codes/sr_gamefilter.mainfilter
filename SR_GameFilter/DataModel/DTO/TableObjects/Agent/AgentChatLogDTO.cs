﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.Agent.AgentChatLogDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0002;
using SR_GameFilter.DataModel.Protocol.Agent;
using System;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects.Agent
{
  public sealed class AgentChatLogDTO
  {
    [DataMember(IsRequired = true, Name = "Time")]
    public DateTime Time
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Chat type")]
    public ChatType ChatType
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Message")]
    public string Message
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Sender char name")]
    public string SenderCharname
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Receiver char name")]
    public string ReceiverCharname
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public AgentChatLogDTO(\u0015 entity, string senderName)
    {
      // ISSUE: unable to decompile the method.
    }

    public AgentChatLogDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
