﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.TableObjects.Agent.AgentChatLogArrayDTO
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.TableObjects.Agent
{
  public sealed class AgentChatLogArrayDTO
  {
    [DataMember(IsRequired = true, Name = "Auto notices")]
    public List<AgentChatLogDTO> Items
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public AgentChatLogArrayDTO(List<AgentChatLogDTO> items)
    {
      // ISSUE: unable to decompile the method.
    }

    public AgentChatLogArrayDTO()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
