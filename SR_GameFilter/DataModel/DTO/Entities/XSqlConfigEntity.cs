﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.Entities.XSqlConfigEntity
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using System;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.Entities
{
  [DataContract]
  public sealed class XSqlConfigEntity
  {
    [NonSerialized]
    internal static GetString \u0099;

    [DataMember(IsRequired = true, Name = "Host")]
    public string Host
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "ID")]
    public string ID
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Password")]
    public string Password
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "SRO_VT_ACCOUNT")]
    public string SRO_VT_ACCOUNT
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "SRO_VT_SHARD")]
    public string SRO_VT_SHARD
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "SRO_VT_SHARDLOG")]
    public string SRO_VT_SHARDLOG
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "SRO_VT_FILTER")]
    public string SRO_VT_FILTER
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "SQL")]
    public static IniFile SQL
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public XSqlConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }

    public static XSqlConfigEntity FromModel()
    {
      // ISSUE: unable to decompile the method.
    }

    static XSqlConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
