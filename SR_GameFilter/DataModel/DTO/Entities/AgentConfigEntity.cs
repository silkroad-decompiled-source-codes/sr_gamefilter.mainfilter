﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.Entities.AgentConfigEntity
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using SR_GameFilter.DataModel.DTO.TableObjects;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.Entities
{
  public sealed class AgentConfigEntity
  {
    [NonSerialized]
    internal static GetString \u009E;

    public ServerConfigDTO ServerConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Net engine config")]
    public NetEngineConfigDTO NetEngineConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Blocked Opcodes")]
    public ExploitArrayDTO Exploit
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Blocked IPs")]
    public BlockedIPsDTO BlockedIPs
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Agent specific")]
    public AgentConfigDTO AgentConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Delays")]
    public AgentDelaysDTO Delays
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Levels")]
    public AgentLevelDTO Levels
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Guild")]
    public AgentGuildMemberDTO Guild
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "GlobalColor")]
    public AgentGlobalsDTO GlobalColor
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Version specific fixes")]
    public VersionSpecificFixesDTO VersionSpecificFixes
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Alchemy config")]
    public AgentAlchemyConfigDTO AlchemyConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Gms config")]
    public GmsRuleArrayDTO AgentGms
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Blocked teleport config")]
    public BlockedTeleportArryDTO BlockedTeleport
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Item Plus Limit config")]
    public PlusLimitArryDTO PlusLimit
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Item adv in item config")]
    public AdvInItemArryDTO AdvInItem
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGConfig")]
    public static IniFile AGConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGDelays")]
    public static IniFile AGDelays
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGLevels")]
    public static IniFile AGLevels
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGNotices")]
    public static IniFile AGNotices
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGAlchemy")]
    public static IniFile AGAlchemy
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGGMS")]
    public static IniFile AGMS
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGGuild")]
    public static IniFile AGGuild
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "AGlobals")]
    public static IniFile AGlobals
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public AgentConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }

    public static AgentConfigEntity FromModel()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Reload()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AgentConfigDTO ConfigDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AgentGlobalsDTO GlobalsDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static NetEngineConfigDTO NetEngine()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ServerConfigDTO Server()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AgentDelaysDTO AgentDelays()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AgentLevelDTO AgentLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AllowedIPsDTO AllowedIps()
    {
      // ISSUE: unable to decompile the method.
    }

    private static BlockedIPsDTO BlockedIPsDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ExploitArrayDTO ExploitArrayDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AgentAlchemyConfigDTO AgentAlchemy()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AgentGuildMemberDTO GuildDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static List<AgentGmsConfigDTO> AgentGmsList()
    {
      // ISSUE: unable to decompile the method.
    }

    private static List<BlockedTeleportDTO> BlockedTeleportDTOs()
    {
      // ISSUE: unable to decompile the method.
    }

    private static List<PlusLimitDTO> GetPlusLimits()
    {
      // ISSUE: unable to decompile the method.
    }

    private static List<byte> AdvInItems()
    {
      // ISSUE: unable to decompile the method.
    }

    static AgentConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
