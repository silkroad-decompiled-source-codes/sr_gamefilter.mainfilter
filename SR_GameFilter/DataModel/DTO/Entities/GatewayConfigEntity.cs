﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.Entities.GatewayConfigEntity
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using SR_GameFilter.DataModel.DTO.TableObjects;
using SR_GameFilter.DataModel.Protocol.Gateway;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.Entities
{
  [DataContract]
  public sealed class GatewayConfigEntity
  {
    [NonSerialized]
    internal static GetString \u0090;

    [DataMember(IsRequired = true, Name = "Server metadata")]
    public ServerConfigDTO ServerConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Server metadata")]
    public ShardArrayDTO Shard
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Net engine configuration")]
    public NetEngineConfigDTO NetEngineConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Redirection rules (both DL and AS)")]
    public RedirectionRuleArrayDTO RedirectionRules
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Blocked IPs")]
    public BlockedIPsDTO BlockedIPs
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Blocked Opcodes")]
    public ExploitArrayDTO Exploit
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Gateway specific")]
    public GatewayConfigDTO GatewayConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Redirection Rules")]
    public static List<RedirectionRuleDTO> Redirection
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "GtConfig")]
    public static IniFile GtConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "RetRedirec")]
    public static IniFile RetRedirec
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Launcher Config")]
    public static IniFile Launcher
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "shards Config")]
    public static IniFile Shards
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Launcher news list")]
    public List<LauncherNewsItem> LauncherNews
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private GatewayConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }

    public static GatewayConfigEntity FromModel()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Reload()
    {
      // ISSUE: unable to decompile the method.
    }

    private static GatewayConfigDTO ConfigDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static NetEngineConfigDTO NetEngine()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ServerConfigDTO Server()
    {
      // ISSUE: unable to decompile the method.
    }

    private static List<RedirectionRuleDTO> ReturnRedirec()
    {
      // ISSUE: unable to decompile the method.
    }

    private static List<ShardDTO> ReturnShards()
    {
      // ISSUE: unable to decompile the method.
    }

    private static List<LauncherNewsItem> NewsItems()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AllowedIPsDTO AllowedIps()
    {
      // ISSUE: unable to decompile the method.
    }

    private static BlockedIPsDTO BlockedIPsDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ExploitArrayDTO ExploitArrayDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    static GatewayConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
