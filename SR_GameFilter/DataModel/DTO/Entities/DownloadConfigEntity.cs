﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.DataModel.DTO.Entities.DownloadConfigEntity
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using SR_GameFilter.DataModel.DTO.TableObjects;
using System;
using System.Runtime.Serialization;

namespace SR_GameFilter.DataModel.DTO.Entities
{
  [DataContract]
  public sealed class DownloadConfigEntity
  {
    [NonSerialized]
    internal static GetString \u0002;

    [DataMember(IsRequired = true, Name = "Server metadata")]
    public ServerConfigDTO ServerConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Net engine configuration")]
    public NetEngineConfigDTO NetEngineConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Blocked IPs")]
    public BlockedIPsDTO BlockedIPs
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Blocked Opcodes")]
    public ExploitArrayDTO Exploit
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "Download server specific")]
    public DownloadConfigDTO DownloadConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DataMember(IsRequired = true, Name = "DWConfig")]
    public static IniFile DWConfig
    {
      get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private DownloadConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }

    public static DownloadConfigEntity FromModel()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Reload()
    {
      // ISSUE: unable to decompile the method.
    }

    private static DownloadConfigDTO ConfigDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static NetEngineConfigDTO NetEngine()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ServerConfigDTO Server()
    {
      // ISSUE: unable to decompile the method.
    }

    private static AllowedIPsDTO AllowedIps()
    {
      // ISSUE: unable to decompile the method.
    }

    private static BlockedIPsDTO BlockedIPsDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ExploitArrayDTO ExploitArrayDTO()
    {
      // ISSUE: unable to decompile the method.
    }

    static DownloadConfigEntity()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
