﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.MainFilter.DataModel.Models.ServerType
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System;

namespace SR_GameFilter.MainFilter.DataModel.Models
{
  [Flags]
  public enum ServerType : byte
  {
    Farm = 0,
    Gateway = 1,
    Download = 2,
    Agent = Download | Gateway, // 0x03
  }
}
