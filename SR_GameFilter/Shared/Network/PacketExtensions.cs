﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.Shared.Network.PacketExtensions
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SR_GameFilter.Shared.Network
{
  public static class PacketExtensions
  {
    private static readonly Dictionary<Type, PacketExtensions.PacketReadHandler> _enumReaders;
    private static readonly Dictionary<Type, PacketExtensions.PacketWriteHandler> _enumWriters;
    [NonSerialized]
    internal static GetString \u0090;

    public static TEnum ReadEnum<TEnum>([In] XPacket obj0) where TEnum : struct, IComparable, IFormattable, IConvertible
    {
      // ISSUE: unable to decompile the method.
    }

    public static void WriteEnum<TEnum>([In] this XPacket obj0, [In] TEnum obj1) where TEnum : struct, IComparable, IFormattable, IConvertible
    {
      // ISSUE: unable to decompile the method.
    }

    static PacketExtensions()
    {
      // ISSUE: unable to decompile the method.
    }

    private delegate object PacketReadHandler([In] XPacket obj0);

    private delegate void PacketWriteHandler([In] XPacket obj0, [In] object obj1);

    private static class \u003C\u003Eo__4<TEnum> where TEnum : struct, IComparable, IFormattable, IConvertible
    {
      public static CallSite<Func<CallSite, object, TEnum>> \u003C\u003Ep__0;
    }

    private static class \u003C\u003Eo__6
    {
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__0;
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__1;
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__2;
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__3;
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__4;
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__5;
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__6;
      public static CallSite<Action<CallSite, XPacket, object>> \u003C\u003Ep__7;
    }

    [Serializable]
    private sealed class \u003C\u003Ec
    {
      public static readonly PacketExtensions.\u003C\u003Ec \u003C\u003E9;
      [NonSerialized]
      internal static GetString \u0087;

      static \u003C\u003Ec()
      {
        // ISSUE: unable to decompile the method.
      }

      public \u003C\u003Ec()
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_0([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_1([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_2([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_3([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_4([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_5([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_6([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal object \u003C\u002Ecctor\u003Eb__6_7([In] XPacket obj0)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_8([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_9([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_10([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_11([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_12([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_13([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_14([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }

      internal void \u003C\u002Ecctor\u003Eb__6_15([In] XPacket obj0, [In] object obj1)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
