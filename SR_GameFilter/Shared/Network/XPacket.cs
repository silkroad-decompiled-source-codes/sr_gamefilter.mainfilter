﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.Shared.Network.XPacket
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SR_GameFilter.Shared.Network
{
  public sealed class XPacket
  {
    private MemoryStream _stream;
    private BinaryReader _reader;
    private BinaryWriter _writer;
    private bool _locked;
    private bool \u003CMassive\u003Ek__BackingField;
    [NonSerialized]
    internal static GetString \u009F;

    public XPacket([In] ushort obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public XPacket([In] ushort obj0, [In] bool obj1)
    {
      // ISSUE: unable to decompile the method.
    }

    public XPacket([In] ushort obj0, [In] bool obj1, [In] bool obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    public XPacket([In] ushort obj0, [In] bool obj1, [In] bool obj2, [In] byte[] obj3, [In] int obj4, [In] int obj5)
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    public ushort get_Opcode()
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    private void set_Opcode([In] ushort obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    public bool get_Encrypted()
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    private void set_Encrypted([In] bool obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    public bool get_Massive()
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    private void set_Massive([In] bool obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    public bool get_HasDataToRead()
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    public long get_Length()
    {
      // ISSUE: unable to decompile the method.
    }

    public T ReadValue<T>()
    {
      // ISSUE: unable to decompile the method.
    }

    public T ReadValue<T>(out bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] ReadByteArray([In] int obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public void WriteValue<T>([In] object obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public void WriteByteArray([In] byte[] obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Lock()
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] GetDataBytes()
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] GetBytes()
    {
      // ISSUE: unable to decompile the method.
    }

    [SpecialName]
    public int get_RemainRead()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SeekToBeginning()
    {
      // ISSUE: unable to decompile the method.
    }

    static XPacket()
    {
      // ISSUE: unable to decompile the method.
    }

    private static class \u003C\u003Eo__31<T>
    {
      public static CallSite<Func<CallSite, object, T>> \u003C\u003Ep__0;
      public static CallSite<Action<CallSite, BinaryWriter, object>> \u003C\u003Ep__1;
    }
  }
}
