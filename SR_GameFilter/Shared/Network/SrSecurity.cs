﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.Shared.Network.SrSecurity
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace SR_GameFilter.Shared.Network
{
  public sealed class SrSecurity
  {
    private static uint[] global_security_table;
    private static Random random;
    private uint m_value_x;
    private uint m_value_g;
    private uint m_value_p;
    private uint m_value_A;
    private uint m_value_B;
    private uint m_value_K;
    private uint m_seed_count;
    private uint m_crc_seed;
    private ulong m_initial_blowfish_key;
    private ulong m_handshake_blowfish_key;
    private byte[] m_count_byte_seeds;
    private ulong m_client_key;
    private ulong m_challenge_key;
    private bool m_client_security;
    private byte m_security_flag;
    private SrSecurity.SecurityFlags m_security_flags;
    private bool m_accepted_handshake;
    private bool m_started_handshake;
    private byte m_identity_flag;
    private string m_identity_name;
    private List<XPacket> m_incoming_packets;
    private List<XPacket> m_outgoing_packets;
    private List<ushort> m_enc_opcodes;
    private Blowfish m_blowfish;
    private TransferBuffer m_recv_buffer;
    private TransferBuffer m_current_buffer;
    private ushort m_massive_count;
    private XPacket m_massive_packet;
    private object m_class_lock;
    [NonSerialized]
    internal static GetString \u0004;

    private static byte FromSecurityFlags([In] SrSecurity.SecurityFlags obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private static SrSecurity.SecurityFlags ToSecurityFlags([In] byte obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private static uint[] GenerateSecurityTable()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ulong MAKELONGLONG_([In] uint obj0, [In] uint obj1)
    {
      // ISSUE: unable to decompile the method.
    }

    private static ushort LOWORD_([In] uint obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private static ushort HIWORD_([In] uint obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private static byte LOBYTE_([In] ushort obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private static byte HIBYTE_([In] ushort obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private static ulong NextUInt64()
    {
      // ISSUE: unable to decompile the method.
    }

    private static uint NextUInt32()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ushort NextUInt16()
    {
      // ISSUE: unable to decompile the method.
    }

    private static byte NextUInt8()
    {
      // ISSUE: unable to decompile the method.
    }

    private uint GenerateValue([In] ref uint obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private void SetupCountByte([In] uint obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private uint G_pow_X_mod_P([In] uint obj0, [In] uint obj1, [In] uint obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void KeyTransformValue([In] ref ulong obj0, [In] uint obj1, [In] byte obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    private byte GenerateCountByte([In] bool obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private byte GenerateCheckByte([In] byte[] obj0, [In] int obj1, [In] int obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    private byte GenerateCheckByte([In] byte[] obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private void GenerateSecurity([In] SrSecurity.SecurityFlags obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private void Handshake([In] ushort obj0, [In] PacketReader obj1, [In] bool obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    private byte[] FormatPacket([In] ushort obj0, [In] byte[] obj1, [In] bool obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    private bool HasPacketToSend()
    {
      // ISSUE: unable to decompile the method.
    }

    private KeyValuePair<TransferBuffer, XPacket> GetPacketToSend()
    {
      // ISSUE: unable to decompile the method.
    }

    public SrSecurity()
    {
      // ISSUE: unable to decompile the method.
    }

    public void GenerateSecurity([In] bool obj0, [In] bool obj1, [In] bool obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Send([In] XPacket obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Recv([In] byte[] obj0, [In] int obj1, [In] int obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Recv([In] TransferBuffer obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<KeyValuePair<TransferBuffer, XPacket>> TransferOutgoing()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<XPacket> TransferIncoming()
    {
      // ISSUE: unable to decompile the method.
    }

    static SrSecurity()
    {
      // ISSUE: unable to decompile the method.
    }

    [StructLayout(LayoutKind.Explicit, Size = 8)]
    private sealed class SecurityFlags
    {
      [FieldOffset(0)]
      public byte none;
      [FieldOffset(1)]
      public byte blowfish;
      [FieldOffset(2)]
      public byte security_bytes;
      [FieldOffset(3)]
      public byte handshake;
      [FieldOffset(4)]
      public byte handshake_response;
      [FieldOffset(5)]
      public byte _6;
      [FieldOffset(6)]
      public byte _7;
      [FieldOffset(7)]
      public byte _8;

      public SecurityFlags()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
