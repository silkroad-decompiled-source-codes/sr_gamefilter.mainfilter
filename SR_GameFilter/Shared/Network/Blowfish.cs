﻿// Decompiled with Syinea's decompiler
// Type: SR_GameFilter.Shared.Network.Blowfish
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using SmartAssembly.Delegates;
using System;
using System.Runtime.InteropServices;

namespace SR_GameFilter.Shared.Network
{
  internal sealed class Blowfish
  {
    private static uint[] bf_P;
    private static uint[,] bf_S;
    private uint[] PArray;
    private uint[,] SBoxes;
    [NonSerialized]
    internal static GetString \u009C;

    public Blowfish()
    {
      // ISSUE: unable to decompile the method.
    }

    private uint S([In] uint obj0, [In] int obj1)
    {
      // ISSUE: unable to decompile the method.
    }

    private uint bf_F([In] uint obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ROUND([In] ref uint obj0, [In] uint obj1, [In] int obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void Blowfish_encipher([In] ref uint obj0, [In] ref uint obj1)
    {
      // ISSUE: unable to decompile the method.
    }

    private void Blowfish_decipher([In] ref uint obj0, [In] ref uint obj1)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Initialize([In] byte[] obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Initialize([In] byte[] obj0, [In] int obj1, [In] int obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetOutputLength([In] int obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] Encode([In] byte[] obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] Encode([In] byte[] obj0, [In] int obj1, [In] int obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] Decode([In] byte[] obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] Decode([In] byte[] obj0, [In] int obj1, [In] int obj2)
    {
      // ISSUE: unable to decompile the method.
    }

    static Blowfish()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
