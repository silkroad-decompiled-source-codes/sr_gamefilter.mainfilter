﻿// Decompiled with Syinea's decompiler
// Type: 3CC00338
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

public class \u0033CC00338
{
  public const int \u003540F12C6 = 32;

  public static uint \u00314652F92(uint _param0, int _param1)
  {
    return _param0 << _param1 | _param0 >> 32 - _param1;
  }

  public static uint \u0033CA059A2(uint _param0, int _param1)
  {
    return _param0 >> _param1 | _param0 << 32 - _param1;
  }

  public static uint \u003564E44A6(uint _param0)
  {
    uint num1 = _param0 & 16711935U;
    uint num2 = _param0 & 4278255360U;
    return (uint) (((int) (num1 >> 8) | (int) num1 << 24) + ((int) num2 << 8 | (int) (num2 >> 24)));
  }
}
