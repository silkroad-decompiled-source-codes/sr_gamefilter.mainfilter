﻿// Decompiled with Syinea's decompiler
// Type: 096B790F
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

public enum \u003096B790F
{
  Success = 0,
  Corrupted = 1,
  Invalid = 2,
  Blacklisted = 4,
  DateExpired = 8,
  RunningTimeOver = 16, // 0x00000010
  BadHwid = 32, // 0x00000020
  MaxBuildExpired = 64, // 0x00000040
}
