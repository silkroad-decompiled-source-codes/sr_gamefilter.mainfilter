﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0006;
using \u0010;
using \u0011;
using \u0017;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0015
{
  [Guid("F7898AF5-CAC4-4632-A2EC-DA06E5111AF2")]
  [TypeLibType(4160)]
  [ComImport]
  public interface \u0001
  {
    [DispId(1)]
    \u0002 LocalPolicy { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

    [DispId(2)]
    \u0002 CurrentProfileType { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; }

    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001();

    [DispId(4)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001(
      [MarshalAs(UnmanagedType.BStr), In] string _param1,
      [In] \u0002 _param2,
      [In] int _param3,
      [MarshalAs(UnmanagedType.BStr), In] string _param4,
      [In] \u0001 _param5,
      [MarshalAs(UnmanagedType.Struct)] out object _param6,
      [MarshalAs(UnmanagedType.Struct)] out object _param7);

    [DispId(5)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001(
      [In] \u0002 _param1,
      [MarshalAs(UnmanagedType.BStr), In] string _param2,
      [In] byte _param3,
      [MarshalAs(UnmanagedType.Struct)] out object _param4,
      [MarshalAs(UnmanagedType.Struct)] out object _param5);
  }
}
