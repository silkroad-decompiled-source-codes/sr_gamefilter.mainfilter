﻿// Decompiled with Syinea's decompiler
// Type: 117076A5
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

internal abstract class \u003117076A5
{
  public const uint \u00305B20FE5 = 12;
  public const int \u00367153F9B = 6;
  private const int \u0031D79087B = 2;
  public const uint \u00348D7160C = 4;
  public const uint \u0030C3D3629 = 2;
  public const int \u003611B3B91 = 4;
  public const uint \u00359FC7649 = 4;
  public const uint \u00377E777E8 = 14;
  public const uint \u00310084B32 = 128;
  public const int \u0036B4841B7 = 4;
  public const uint \u00357FF4F59 = 16;
  public const int \u00365E96AD1 = 3;
  public const int \u00306FE63BC = 3;
  public const int \u00363E461B5 = 8;
  public const uint \u00369121D80 = 8;
  public const uint \u00379D82C58 = 8;

  public static uint \u003778E182A(uint _param0)
  {
    _param0 -= 2U;
    return _param0 < 4U ? _param0 : 3U;
  }

  public struct \u0034E4866AF
  {
    public uint \u0034F7803B9;

    public void \u0033D336971()
    {
      this.\u0034F7803B9 = 0U;
    }

    public void \u00368876648()
    {
      if (this.\u0034F7803B9 < 4U)
        this.\u0034F7803B9 = 0U;
      else if (this.\u0034F7803B9 < 10U)
        this.\u0034F7803B9 -= 3U;
      else
        this.\u0034F7803B9 -= 6U;
    }

    public void \u00328AB6880()
    {
      this.\u0034F7803B9 = this.\u0034F7803B9 < 7U ? 7U : 10U;
    }

    public void \u00326347EED()
    {
      this.\u0034F7803B9 = this.\u0034F7803B9 < 7U ? 8U : 11U;
    }

    public void \u00362F4413D()
    {
      this.\u0034F7803B9 = this.\u0034F7803B9 < 7U ? 9U : 11U;
    }

    public bool \u00348754900()
    {
      return this.\u0034F7803B9 < 7U;
    }
  }
}
