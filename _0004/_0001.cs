﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0008;
using \u0017;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0004
{
  [Guid("D4BECDDF-6F73-4A83-B832-9C66874CD20E")]
  [TypeLibType(4160)]
  [ComImport]
  public interface \u0001
  {
    [DispId(1)]
    \u0002 IpVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(2)]
    \u0001 Scope { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(3)]
    string RemoteAddresses { [MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

    bool Enabled { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
  }
}
