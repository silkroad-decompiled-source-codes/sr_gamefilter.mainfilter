﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0010;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0011
{
  [Guid("D46D2478-9AC9-4008-9DC7-5563CE5536CC")]
  [TypeLibType(4160)]
  [ComImport]
  public interface \u0002
  {
    [DispId(1)]
    \u0001 CurrentProfile { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

    [MethodImpl(MethodImplOptions.InternalCall)]
    [return: MarshalAs(UnmanagedType.Interface)]
    \u0001 \u0001([In] \u0002 _param1);
  }
}
