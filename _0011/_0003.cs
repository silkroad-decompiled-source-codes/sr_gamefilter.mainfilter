﻿// Decompiled with Syinea's decompiler
// Type: .
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using \u0004;
using \u0012;
using \u0014;
using \u0016;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0011
{
  [ComImport]
  public interface \u0003
  {
    int CurrentProfileTypes { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

    bool FirewallEnabled { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    object ExcludedInterfaces { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Struct)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.Struct)] set; }

    [DispId(4)]
    bool BlockAllInboundTraffic { [MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(5)]
    bool NotificationsDisabled { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

    bool UnicastResponsesToMulticastBroadcastDisabled { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

    \u0001 Rules { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

    \u0001 ServiceRestriction { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

    \u0005 DefaultInboundAction { [DispId(12), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(12), MethodImpl(MethodImplOptions.InternalCall)] set; }

    \u0005 DefaultOutboundAction { [DispId(13), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(13), MethodImpl(MethodImplOptions.InternalCall)] set; }

    [DispId(14)]
    bool IsRuleGroupCurrentlyEnabled { [DispId(14), MethodImpl(MethodImplOptions.InternalCall)] get; }

    \u0001 LocalPolicyModifyState { [DispId(15), MethodImpl(MethodImplOptions.InternalCall)] get; }

    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001([In] int _param1, [MarshalAs(UnmanagedType.BStr), In] string _param2, [In] bool _param3);

    [DispId(10)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    bool \u0001([In] int _param1, [MarshalAs(UnmanagedType.BStr), In] string _param2);

    [DispId(11)]
    [MethodImpl(MethodImplOptions.InternalCall)]
    void \u0001();
  }
}
