﻿// Decompiled with Syinea's decompiler
// Type: 037B2395
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System.IO;

public class \u003037B2395
{
  private uint \u0030DBC6294 = 1;
  private byte[] \u0036B767518;
  private uint \u00376664DCD;
  private uint \u00353D20283;
  private uint \u00343DF286D;
  private Stream \u0030E0E3BBA;
  public uint \u0032FA3211E;

  public void \u003516A2AEE(uint _param1)
  {
    if ((int) this.\u00353D20283 != (int) _param1)
      this.\u0036B767518 = new byte[(int) _param1];
    this.\u00353D20283 = _param1;
    this.\u00376664DCD = 0U;
    this.\u00343DF286D = 0U;
  }

  public void \u0030D0B13A8(Stream _param1, bool _param2)
  {
    this.\u0034E25741E();
    this.\u0030E0E3BBA = _param1;
    if (_param2)
      return;
    this.\u00343DF286D = 0U;
    this.\u00376664DCD = 0U;
    this.\u0032FA3211E = 0U;
  }

  public void \u0034E25741E()
  {
    this.\u00300932405();
    this.\u0030E0E3BBA = (Stream) null;
  }

  public void \u00300932405()
  {
    uint num = this.\u00376664DCD - this.\u00343DF286D;
    if (num == 0U)
      return;
    this.\u0030E0E3BBA.Write(this.\u0036B767518, (int) this.\u00343DF286D, (int) num);
    if (this.\u00376664DCD >= this.\u00353D20283)
      this.\u00376664DCD = 0U;
    this.\u00343DF286D = this.\u00376664DCD;
  }

  public void \u00353715B56(uint _param1, uint _param2)
  {
    uint num = (uint) ((int) this.\u00376664DCD - (int) _param1 - 1);
    if (num >= this.\u00353D20283)
      num += this.\u00353D20283;
    for (; _param2 > 0U; --_param2)
    {
      if (num >= this.\u00353D20283)
        num = 0U;
      this.\u0036B767518[(int) this.\u00376664DCD++] = this.\u0036B767518[(int) num++];
      if (this.\u00376664DCD >= this.\u00353D20283)
        this.\u00300932405();
    }
  }

  public void \u00316DA69A4(byte _param1)
  {
    this.\u0036B767518[(int) this.\u00376664DCD++] = _param1;
    if (this.\u00376664DCD < this.\u00353D20283)
      return;
    this.\u00300932405();
  }

  public byte \u003247250F8(uint _param1)
  {
    uint num = (uint) ((int) this.\u00376664DCD - (int) _param1 - 1);
    if (num >= this.\u00353D20283)
      num += this.\u00353D20283;
    return this.\u0036B767518[(int) num];
  }
}
