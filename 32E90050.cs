﻿// Decompiled with Syinea's decompiler
// Type: 32E90050
// Assembly: SR_GameFilter.MainFilter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C299649D-AA73-49D1-A4AB-9DABEAF7036A
// Assembly location: C:\Users\Syinea\Downloads\SrGameFilter v1 (1)\SrGameFilter v1\SR_GameFilter.MainFilter.exe

using System;
using System.Runtime.InteropServices;

public class \u00332E90050
{
  private static uint[] \u00378A13805;

  public \u00332E90050()
  {
    if (\u00332E90050.\u00378A13805 != null)
      return;
    \u00332E90050.\u00378A13805 = new uint[256];
    for (int index1 = 0; index1 < 256; ++index1)
    {
      uint num = (uint) index1;
      for (int index2 = 0; index2 < 8; ++index2)
      {
        if (((int) num & 1) == 1)
          num = num >> 1 ^ 3988292384U;
        else
          num >>= 1;
      }
      \u00332E90050.\u00378A13805[index1] = num;
    }
  }

  public uint \u0035B4709CA(IntPtr _param1, uint _param2)
  {
    uint num = 0;
    for (int index = 0; (long) index < (long) _param2; ++index)
      num = \u00332E90050.\u00378A13805[((int) Marshal.ReadByte(new IntPtr(_param1.ToInt64() + (long) index)) ^ (int) num) & (int) byte.MaxValue] ^ num >> 8;
    return ~num;
  }
}
